package nl.yougo.apps.qrapp.Activities;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.provider.ContactsContract;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.ResultPoint;
import com.google.zxing.client.android.BeepManager;
import com.journeyapps.barcodescanner.BarcodeCallback;
import com.journeyapps.barcodescanner.BarcodeResult;
import com.journeyapps.barcodescanner.DecoratedBarcodeView;

import java.net.URL;
import java.util.List;

import nl.yougo.apps.qrapp.R;

public class ReadActivity extends AppCompatActivity {

    private static final String TAG = "READ_QR_CODE";
    private DecoratedBarcodeView barcodeView;
    private BeepManager beepManager;
    private String lastText;
    private TextView mTextView;
    private static final int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 88;
    private Context mContext;
    private boolean requestAccepted = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_read);
        mContext = this;

        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {

                // permission has not been granted, ask it.
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.CAMERA},
                        MY_PERMISSIONS_REQUEST_READ_CONTACTS);
        } else {
            // permission has already been granted
            requestAccepted = true;
        }

        barcodeView = (DecoratedBarcodeView) findViewById(R.id.barcode_scanner);
        mTextView = (TextView) findViewById(R.id.permission_denied);
        beepManager = new BeepManager(this);
        barcodeView.decodeContinuous(callback);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_CONTACTS: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    barcodeView.resume();
                    requestAccepted = true;
                } else {
                    barcodeView.pause();
                    barcodeView.setVisibility(View.GONE);
                    mTextView.setVisibility(View.VISIBLE);
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    private BarcodeCallback callback = new BarcodeCallback() {
        @Override
        public void barcodeResult(BarcodeResult result) {
            if(result.getText() == null || result.getText().equals(lastText)) {
                // Prevent duplicate scans
                return;
            }

            lastText = result.getText();
            barcodeView.setStatusText(result.getText());
            beepManager.playBeepSoundAndVibrate();


            if(validateHTTP_URI(lastText)) {
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(lastText));
                startActivity(i);
            } else {
                Toast.makeText(mContext, "URL malformed", Toast.LENGTH_SHORT).show();
                Log.e(TAG, "Tried to parse wrong uri");
            }

        }

        @Override
        public void possibleResultPoints(List<ResultPoint> resultPoints) {
        }
    };

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        return barcodeView.onKeyDown(keyCode, event) || super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(requestAccepted) {
            barcodeView.resume();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(requestAccepted) {
            barcodeView.pause();
        }
    }

    private boolean validateHTTP_URI(String uri) {
        final URL url;
        try {
            url = new URL(uri);
        } catch (Exception e1) {
            return false;
        }
        return "http".equals(url.getProtocol());
    }
}
