package nl.yougo.apps.qrapp.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import nl.yougo.apps.qrapp.R;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button createButton = (Button) findViewById(R.id.create_btn);
        createButton.setOnClickListener(this);

        Button readButton = (Button) findViewById(R.id.read_btn);
        readButton.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        Intent activityIntent = null;
        if(view.getId() == R.id.create_btn) {
            // Create QR Code button
            activityIntent = new Intent(MainActivity.this, CreateActivity.class);

        } else if(view.getId() == R.id.read_btn) {
            activityIntent = new Intent(MainActivity.this, ReadActivity.class);
        }

        if(null != activityIntent) {
            startActivity(activityIntent);
        }

    }
}
