package nl.yougo.apps.qrapp.Activities;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.common.StringUtils;
import com.google.zxing.qrcode.QRCodeWriter;

import nl.yougo.apps.qrapp.R;

public class CreateActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText mUrlText;
    private Button mCreateQRBtn;
    private ImageView mQRCodeImage;
    private final String TAG = "CREATE_QR_CODE";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create);

        mUrlText = (EditText) findViewById(R.id.qr_link);
        mCreateQRBtn = (Button) findViewById(R.id.make_qr);
        mQRCodeImage = (ImageView) findViewById(R.id.qr_code_bitmap);

        mCreateQRBtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.make_qr) {
            // get url and make qr
            String url = mUrlText.getText().toString();
            if(!TextUtils.isEmpty(url)) {
                try {
                    mQRCodeImage.setImageBitmap(generateQrCode(url));
                    mQRCodeImage.setVisibility(View.VISIBLE);
                } catch (WriterException e) {
                    Log.e(TAG, e.getMessage());
                }
            } else {
                Toast.makeText(this, "Sorry, but no value was given", Toast.LENGTH_SHORT).show();
            }
        }
    }


    public Bitmap generateQrCode(String input) throws WriterException {
        QRCodeWriter writer = new QRCodeWriter();
        try {
            BitMatrix bitMatrix = writer.encode(input, BarcodeFormat.QR_CODE, 512, 512);
            int width = bitMatrix.getWidth();
            int height = bitMatrix.getHeight();
            Bitmap bmp = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565);
            for (int x = 0; x < width; x++) {
                for (int y = 0; y < height; y++) {
                    bmp.setPixel(x, y, bitMatrix.get(x, y) ? Color.BLACK : Color.WHITE);
                }
            }
            return bmp;

        } catch (WriterException e) {
            Log.e(TAG, e.getMessage());
        }

        return null;
    }
}
